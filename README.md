# README #

### Quick summary ###
  This repo is a quick demonstration of how terraform can be used to create various AWS resources by describing everything in code and invoking the creation with a single command.

### How do I get set up? ###

#### Summary of set up ####
  Following resources will be created at the end of this demo:

* A VPC network
* A public subnet
* A private subnet
* A NAT server
* A security group to allow ssh and HTTP access to the EC2 instance
* A EC2 instance with Jenkins installed with port redirection from port 8080 to port 80
* A key pair registered into AWS to be used for connecting to the EC2 instance

#### Prerequisites ####
  Following items are needed to work with the repository:

* An AWS Access Key and Secret 
* A ssh keypair generated locally using ssh-keygen 
* Terraform installed on your environment as per [this link ](https://www.terraform.io/intro/getting-started/install.html)


#### Steps to run this Demo ####

* Verify terraform is working using command `terraform` The output must be like below 
~~~
$ terraform
usage: terraform [--version] [--help] <command> [args]

The available commands for execution are listed below.
The most common, useful commands are shown first, followed by
less common or more advanced commands. If you're just getting
started with Terraform, stick with the common commands. For the
other commands, please read the help and docs before usage.

Common commands:
    apply              Builds or changes infrastructure
    destroy            Destroy Terraform-managed infrastructure
    fmt                Rewrites config files to canonical format
    get                Download and install modules for the configuration
    graph              Create a visual graph of Terraform resources
    import             Import existing infrastructure into Terraform
    init               Initializes Terraform configuration from a module
    output             Read an output from a state file
    plan               Generate and show an execution plan
    push               Upload this Terraform module to Atlas to run
    refresh            Update local state file against real resources
    remote             Configure remote state storage
    show               Inspect Terraform state or plan
    taint              Manually mark a resource for recreation
    untaint            Manually unmark a resource as tainted
    validate           Validates the Terraform files
    version            Prints the Terraform version

All other commands:
    state              Advanced state management
~~~
* Copy this repo locally using command `git clone https://harshal-shah@bitbucket.org/harshal-shah/terraform-jenkins.git`
* cd into the terraform-jenkins directory
* Copy your public and private ssh keys to the ssh folder
* Create a file called **terraform.tfvars** and edit it using your favourite text editor.
* Add following lines to **terraform.tfvars**  You can choose region among ap-southeast-1 , us-west-1 , us-east-1 , eu-central-1 
~~~
access_key = "<Your AWS Access Key>"
secret_key = "<Your AWS Secret Key>"
region = "ap-southeast-1" 
aws_public_key = "<Your Public Key Contents>"
aws_private_key_name = "<Name of your private key>"
~~~
 
* Run `terraform plan` to see if everything works fine. This shows you the resources that terraform will create when you run `apply` 
* Now run `terraform apply` for terraform to create everything mentioned above.
* At the end of execution you will get output which gives the public IP of the EC2 instance on which jenkins is installed.
* Verify whether it is working fine by going to `http://<PUBLIC IP of the EC2 instance>`