/* App servers */
resource "aws_instance" "app" {
  count = 1
  ami = "${lookup(var.amis, var.region)}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.public.id}"
  security_groups = ["${aws_security_group.web.id}"]
  key_name = "${aws_key_pair.deployer.key_name}"
  source_dest_check = false
  
  tags = { 
    Name = "terraform-jenkins"
  }
  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = "${file("ssh/${aws_key_pair.deployer.key_name}")}"
    timeout = "2m"
    agent = false
	}  
  provisioner "file" {
    source = "installJenkins.sh"
    destination = "/tmp/installJenkins.sh"
  }

  provisioner "remote-exec" {
    inline = [
    "chmod +x /tmp/installJenkins.sh",
    "/tmp/installJenkins.sh"
    ]
  }
}
